 <footer class="site-footer">
     <div class="text-center">
         <b class="copyright">&copy; Powered by RAD FINKORP. </b> All rights reserved.
         <a href="#" class="go-top">
             <i class="fa fa-angle-up"></i>
         </a>
     </div>
 </footer>